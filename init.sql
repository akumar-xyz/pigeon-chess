CREATE DATABASE pigeon_chess_database;

use pigeon_chess_database;

CREATE TABLE users (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	name VARCHAR(255) NOT NULL,
	email VARCHAR(50) NOT NULL,
	password varchar(255),
	ELO integer(5),
);
